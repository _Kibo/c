﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Labs
{
    class ChangePersonCmd:AbstractCommand
    {
        private Person _personForChange;

        private string _currName;
        private string _currLastName;
        private int _currAge;
        private string _currCity;

        private string _newName;
        private string _newLastName;
        private int _newAge;
        private string _newCity;


        public ChangePersonCmd(Person person, string name, string lastName, int age, string city)
        {
            _currName = person.Name;
            _currLastName = person.LastName;
            _currAge = person.Age;
            _currCity = person.City;

            _personForChange = person;
            _newName = name;
            _newLastName = lastName;
            _newAge = age;
            _newCity = city;
        }

        public override void doit()
        {
            _personForChange.Name = _newName;
            _personForChange.LastName = _newLastName;
            _personForChange.Age = _newAge;
            _personForChange.City = _newCity;

            _personForChange.updateTreeText();
            AppForm.getAppForm().MyTreeView.SelectedNode = _personForChange;
        }

        public override void undo()
        {
            _personForChange.Name = _currName;
            _personForChange.LastName = _currLastName;
            _personForChange.Age = _currAge;
            _personForChange.City = _currCity;

            _personForChange.updateTreeText();
            AppForm.getAppForm().MyTreeView.SelectedNode = _personForChange;
        }

        public override void redo()
        {
            doit();
        }
    }

}
