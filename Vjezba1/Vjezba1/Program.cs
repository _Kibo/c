﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba1
{
    class Zadatak1
    {

        static void Main(string[] args)
        {
            Console.Write("Prvi: ");
            string prvi = Console.ReadLine();
            Console.Write("Drugi: ");
            string drugi = Console.ReadLine();
            try
            {
                int rezultat = System.Convert.ToInt32(prvi) / System.Convert.ToInt32(drugi);
                Console.WriteLine("int:" + rezultat);
                Console.WriteLine("Currency: {0:C}", rezultat);
                Console.WriteLine("Number: {0:N}", rezultat);
                Console.WriteLine("Fixed-point: {0:F}", rezultat);
                Console.WriteLine("General: {0:G}", rezultat);
                Console.WriteLine("Hexadecimal: {0:X}", rezultat);
                Console.WriteLine("Scientific: {0:E}", rezultat);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
