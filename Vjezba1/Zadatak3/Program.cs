﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    public enum AccountType
    {
        Stednja = 1,
        Tekuci_racun = 2,
        Ziro_racun = 3
    }
    public class BankAccount
    {
        public int broj_racuna;
        public double stanje_racuna;
        public AccountType Tip;
        public BankAccount(int a, double b, AccountType c)
        {
            broj_racuna = a;
            stanje_racuna = b;
            Tip = c;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<BankAccount> Racuni = new List<BankAccount>();
            Racuni.Add(new BankAccount(1, 255.2, AccountType.Tekuci_racun));
            Racuni.Add(new BankAccount(2, 1000, AccountType.Ziro_racun));
            Racuni.Add(new BankAccount(3, 0.17, AccountType.Tekuci_racun));
            Racuni.Add(new BankAccount(4, 10000, AccountType.Stednja));
            Racuni.Add(new BankAccount(5, 5600, AccountType.Stednja));


            Console.WriteLine("Unesi odabir(1-novi racun, 2-ispis racuna 3-kraj:");
            int choice = Convert.ToInt32(Console.ReadLine());

            while (choice == 1 || choice == 2)
            {
                if (choice == 1)
                {
                    Console.WriteLine("Unesi broj racuna:");
                    int broj_racuna = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Unesi stanje racuna:");
                    double stanje_racuna = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine($"Odaberi tip racuna({(int)AccountType.Tekuci_racun}-tekuci, {(int)AccountType.Ziro_racun}-ziro, {(int)AccountType.Stednja}-stednja): ");
                    int tip_racuna = Convert.ToInt32(Console.ReadLine());
                    if (Enum.IsDefined(typeof(AccountType), tip_racuna))
                    {
                        Racuni.Add(new BankAccount(broj_racuna, stanje_racuna, (AccountType)tip_racuna));
                    }
                }
                if (choice == 2)
                {
                    foreach (BankAccount Racun in Racuni)
                    {
                        Console.WriteLine("Broj Racuna: {0}, Stanje Racuna: {1}, Tip racuna: {2}", Racun.broj_racuna, Racun.stanje_racuna, Racun.Tip);
                    }
                }
                Console.WriteLine("Unesi odabir(1-novi racun, 2-ispis racuna 3-kraj:");
                choice = Convert.ToInt32(Console.ReadLine());
            }
        }
    }