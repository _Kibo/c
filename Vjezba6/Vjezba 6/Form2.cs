﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba_6
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Person person, string str)
        {
            if (str == "Details")
            {
                InitializeComponent();
                NameInput.Text = person._name;
                LastNameInput.Text = person._lastName;
                AgeInput.Text = Convert.ToString(person._age);
                CityInput.Text = person._city;
                NameInput.Enabled = false;
                LastNameInput.Enabled = false;
                CityInput.Enabled = false;
                AgeInput.Enabled = false;
            }
            if (str == "Edit")
            {
                InitializeComponent();
                NameInput.Text = person._name;
                LastNameInput.Text = person._lastName;
                AgeInput.Text = Convert.ToString(person._age);
                CityInput.Text = person._city;
            }
        }

        public string getNameInput()
        {
            return NameInput.Text.Trim();
        }

        public string getLastNameInput()
        {
            return LastNameInput.Text.Trim();
        }

        public int getAgeInput()
        {
            return Convert.ToInt32(AgeInput.Text.Trim());
        }

        public string getCityInput()
        {
            return CityInput.Text.Trim();
        }

        private void InputCheck(object sender, EventArgs e)
        {
            if (int.TryParse(AgeInput.Text, out int val) &&
                NameInput.Text.Trim().Length > 0 &&
                LastNameInput.Text.Trim().Length > 0 &&
                Convert.ToInt32(AgeInput.Text.Trim()) > 0 &&
                CityInput.Text.Length > 0)
            {
                OkButton.Enabled = true;
            }
            else
            {
                OkButton.Enabled = false;
            }
        }
    }

}
