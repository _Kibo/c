﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            Form2 personForm = new Form2();
            personForm.ShowDialog(this);

            if (personForm.DialogResult == DialogResult.OK)
            {
                Person person = new Person(personForm.getNameInput(), personForm.getLastNameInput(),
                    personForm.getCityInput(), personForm.getAgeInput());

                PeopleModel.AddPerson(person);
                ListViewItem newItem = new ListViewItem(person._name);
                newItem.Tag = person;
                newItem.SubItems.Add(person._lastName);
                listView1.Items.Add(newItem);

                personForm.Dispose();
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            List<Person> people = PeopleModel.GetAllPeople();

            foreach(Person person in people)
            {
                ListViewItem newItem = new ListViewItem(person._name);
                newItem.SubItems.Add(person._lastName);
                listView1.Items.Add(newItem);
            }
        }


        private void peopleListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listView1.Focused)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void detailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Person person = (Person)listView1.FocusedItem.Tag;
            Form2 form2 = new Form2(person, "Details");
            form2.ShowDialog(this);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Person person = (Person)listView1.SelectedItems[0].Tag;
            Form2 form2 = new Form2(person, "Edit");
            form2.ShowDialog(this);
            if (form2.DialogResult == DialogResult.OK)
            {
                person._name = form2.getNameInput();
                person._lastName = form2.getLastNameInput();
                person._city = form2.getCityInput();
                person._age = form2.getAgeInput();

                listView1.FocusedItem.SubItems.Clear();
                listView1.FocusedItem.Text = person._name;
                listView1.FocusedItem.SubItems.Add(person._lastName);
                listView1.FocusedItem.Tag = person;

                form2.Dispose();
            }
        }
    }
}
