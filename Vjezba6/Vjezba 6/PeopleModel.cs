﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba_6
{
    class PeopleModel
    {
        private static List<Person> people = new List<Person>();


        public PeopleModel()
        {

        }

        public static List<Person> GetAllPeople()
        {
            return people;
        }

        public static void AddPerson(Person person)
        {
            people.Add(person);
        }
    }
}
