﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Labs
{
    [Serializable()]
    public class SerializationData //: ISerializable
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int xPos { get; set; }
        public int yPos { get; set; }

        /*public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Width", Width);
            info.AddValue("Height", Height);
            info.AddValue("xPos", xPos);
            info.AddValue("yPos", yPos);

        }*/
    }
}
