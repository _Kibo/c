﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Labs
{
    public partial class SummaryViewForm : Form
    {
        private int split_count;
        private int zagreb_count;
        private int rijeka_count;

        public SummaryViewForm()
        {
            split_count = 0;
            zagreb_count = 0;
            rijeka_count = 0;
            InitializeComponent();
            PersonDataModel.getDataModel().PersonModelChanged += new PersonModelChangedEventHandler(this.consumeChangeInPersonDataModel);
        }


        private void SummaryViewForm_Load(object sender, EventArgs e)
        {
            foreach (Person p in PersonDataModel.getDataModel().getAllPersons())
            {
                if (p.City == "Split")
                {
                    split_count++;
                }
                else if (p.City == "Zagreb")
                {
                    zagreb_count++;
                }
                else if (p.City == "Rijeka")
                {
                    rijeka_count++;
                }
            }
            label4.Text = Convert.ToString(split_count);
            label5.Text = Convert.ToString(zagreb_count);
            label6.Text = Convert.ToString(rijeka_count);
        }

        private void consumeChangeInPersonDataModel(object sender, PersonDataModelChangedEventArgs e)
        {
            Person p = e.PersonInChange;
            if (e.IsAdded)
            {
                if (p.City == "Rijeka")
                {
                    this.rijeka_count += 1;
                    label6.Text = Convert.ToString(rijeka_count);
                }
                if (p.City == "Zagreb")
                {
                    this.zagreb_count += 1;
                    label5.Text = Convert.ToString(zagreb_count);
                }
                if (p.City == "Split")
                {
                    this.split_count += 1;
                    label4.Text = Convert.ToString(split_count);
                }

            }
            else if (e.IsRemoved)
            {
                if (p.City == "Rijeka")
                {
                    this.rijeka_count -= 1;
                    label6.Text = Convert.ToString(rijeka_count);
                }
                if (p.City == "Zagreb")
                {
                    this.zagreb_count -= 1;
                    label5.Text = Convert.ToString(zagreb_count);
                }
                if (p.City == "Split")
                {
                    this.split_count -= 1;
                    label4.Text = Convert.ToString(split_count);
                }
            }
        }
    }
}
