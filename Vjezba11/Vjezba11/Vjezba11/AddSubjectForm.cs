﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba11
{
    public partial class AddSubjectForm : Form
    {
        public AddSubjectForm()
        {
            InitializeComponent();
        }
        public AddSubjectForm(Predmeti predmet, bool detailsCheck)
        {
            InitializeComponent();
            if (detailsCheck == true)
            {
                SubjectNameInput.Enabled = false;
                listView1.Enabled = false;
            }

            SubjectNameInput.Text = predmet.Naziv;
            var context = new Entities();
            var studenti = from p in context.Predmetis
                           where (p.Id == predmet.Id)
                           from s in p.Studentis
                           select s;

            foreach (Studenti student in studenti)
            {
                ListViewItem newItem = new ListViewItem(student.Ime);
                newItem.SubItems.Add(student.Prezime);
                newItem.Tag = predmet;
                listView1.Items.Add(newItem);
            }
        }
        public string getNameInput()
        {
            return SubjectNameInput.Text.Trim();
        }
    }
}
