﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba11
{
    public partial class EnrollSubjectForm : Form
    {
        Studenti student;
        public EnrollSubjectForm(Studenti _student)
        {
            student = _student;
            InitializeComponent();
        }

        public string getSubjectInput()
        {
            return comboBox1.Text.Trim();
        }

        private void EnrollSubjectForm_Load(object sender, EventArgs e)
        {
            var context = new Entities();
            var predmeti = context.Predmetis;
            var _student = context.Studentis.SingleOrDefault(s => s.Id == student.Id);
            List<Predmeti> dataSource = new List<Predmeti>();
            foreach (Predmeti predmet in predmeti)
            {
                if (_student.Predmetis.Contains(predmet) == false)
                {
                    dataSource.Add(new Predmeti() { Naziv = predmet.Naziv });
                }
            }
            comboBox1.DataSource = dataSource;
            comboBox1.DisplayMember = "Naziv";
        }
    }
}
