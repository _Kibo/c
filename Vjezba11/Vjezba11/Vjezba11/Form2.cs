﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba11
{
    public partial class studentForm : Form
    {
        private Studenti studentInChange;

        public studentForm()
        {
            InitializeComponent();
            studentInChange = null;
            listView1.Enabled = false;
            button1.Enabled = false;
        }

        public studentForm(Studenti student, bool detailsCheck)
        {
            studentInChange = student;

            InitializeComponent();
            if (detailsCheck == true)
            {
                NameInput.Enabled = false;
                LastNameInput.Enabled = false;
                listView1.Enabled = false;
                button1.Enabled = false;
            }

            NameInput.Text = student.Ime;
            LastNameInput.Text = student.Prezime;
            var context = new Entities();
            var predmeti = from s in context.Studentis where (s.Id == student.Id)
                           from p in s.Predmetis
                           select p;

            //var predmeti = student.Predmetis;
            foreach(Predmeti predmet in predmeti)
            {
                ListViewItem newItem = new ListViewItem(predmet.Naziv);
                newItem.Tag = predmet;
                listView1.Items.Add(newItem);
            }
        }

        public string getNameInput()
        {
            return NameInput.Text.Trim();
        }

        public string getLastNameInput()
        {
            return LastNameInput.Text.Trim();
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listView1.Focused)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EnrollSubjectForm enrollSubjectForm = new EnrollSubjectForm(studentInChange);
            enrollSubjectForm.ShowDialog(this);
            if(enrollSubjectForm.DialogResult == DialogResult.OK)
            {
                var context = new Entities();
                string predmetNaziv = enrollSubjectForm.getSubjectInput();
                Predmeti predmet = context.Predmetis.SingleOrDefault(p => p.Naziv == predmetNaziv);
                var student = context.Studentis.First(s => s.Id == studentInChange.Id);
                student.Predmetis.Add(predmet);
                context.SaveChanges();
                ListViewItem newItem = new ListViewItem(predmet.Naziv);
                newItem.Tag = predmet;
                listView1.Items.Add(newItem);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Predmeti predmet = (Predmeti)listView1.FocusedItem.Tag;
            var context = new Entities();
            Predmeti predmet_remove = context.Predmetis.SingleOrDefault(p => p.Id == predmet.Id);
            Studenti student_remove = context.Studentis.SingleOrDefault(s => s.Id == studentInChange.Id);
            student_remove.Predmetis.Remove(predmet_remove);
            context.SaveChanges();
            listView1.FocusedItem.Remove();
        }
    }
}
