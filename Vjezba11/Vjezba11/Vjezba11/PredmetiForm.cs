﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba11
{
    public partial class PredmetiForm : Form
    {
        public PredmetiForm()
        {
            InitializeComponent();
        }

        private void PredmetiForm_Load(object sender, EventArgs e)
        {
            var context = new Entities();
            var predmeti = context.Predmetis;

            foreach (Predmeti predmet in predmeti)
            {
                ListViewItem newItem = new ListViewItem(Convert.ToString(predmet.Id));
                newItem.SubItems.Add(predmet.Naziv);
                newItem.Tag = predmet;
                listView1.Items.Add(newItem);
            }
        }

        private void newSubjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSubjectForm addSubjectForm = new AddSubjectForm();
            addSubjectForm.ShowDialog(this);
            if (addSubjectForm.DialogResult == DialogResult.OK)
            {
                Predmeti predmet = new Predmeti();
                predmet.Naziv = addSubjectForm.getNameInput();
                var context = new Entities();
                context.Predmetis.Add(predmet);
                context.SaveChanges();
                ListViewItem newItem = new ListViewItem(Convert.ToString(predmet.Id));
                newItem.Tag = predmet;
                newItem.SubItems.Add(predmet.Naziv);
                listView1.Items.Add(newItem);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Predmeti predmet = (Predmeti)listView1.FocusedItem.Tag;
            var context = new Entities();
            var predmet_delete = context.Predmetis.SingleOrDefault(p => p.Id == predmet.Id);
            foreach (Studenti student in predmet_delete.Studentis)
            {
                predmet.Studentis.Remove(student);
            }
            context.Predmetis.Remove(predmet_delete);
            context.SaveChanges();
            listView1.FocusedItem.Remove();
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listView1.Focused)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Predmeti predmet = (Predmeti)listView1.FocusedItem.Tag;
            AddSubjectForm subjectForm = new AddSubjectForm(predmet, false);
            subjectForm.ShowDialog(this);

            if (subjectForm.DialogResult == DialogResult.OK)
            {
                var context = new Entities();
                var predmetInChange = context.Predmetis.First(p => p.Id == predmet.Id);
                predmetInChange.Naziv = subjectForm.getNameInput();
                context.SaveChanges();
                listView1.FocusedItem.SubItems.Clear();
                listView1.FocusedItem.SubItems.Add(subjectForm.getNameInput());
                listView1.FocusedItem.Tag = predmet;
            }
        }

        private void detailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Predmeti predmet = (Predmeti)listView1.FocusedItem.Tag;
            AddSubjectForm subjectForm = new AddSubjectForm(predmet, true);
            subjectForm.ShowDialog(this);
        }
    }
}
