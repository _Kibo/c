﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjezba11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var context = new Entities();
            var studenti = context.Studentis;

            foreach (Studenti student in studenti)
            {
                ListViewItem newItem = new ListViewItem(Convert.ToString(student.Id));
                newItem.SubItems.Add(student.Ime);
                newItem.SubItems.Add(student.Prezime);
                newItem.Tag = student;
                listView1.Items.Add(newItem);
            }
        }

        private void newStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            studentForm studentForm = new studentForm();
            studentForm.ShowDialog(this);
            if (studentForm.DialogResult == DialogResult.OK)
            {
                try
                {
                    Studenti student = new Studenti();
                    student.Ime = studentForm.getNameInput();
                    student.Prezime = studentForm.getLastNameInput();
                    var context = new Entities();
                    context.Studentis.Add(student);
                    context.SaveChanges();
                    ListViewItem newItem = new ListViewItem(Convert.ToString(student.Id));
                    newItem.Tag = student;
                    newItem.SubItems.Add(student.Ime);
                    newItem.SubItems.Add(student.Prezime);
                    listView1.Items.Add(newItem);
                }
                catch
                {

                }
            }
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listView1.Focused)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Studenti student = (Studenti)listView1.FocusedItem.Tag;
            var context = new Entities();
            var student_delete = context.Studentis.SingleOrDefault(s => s.Id == student.Id);
            foreach (Predmeti predmet in student_delete.Predmetis)
            {
                student.Predmetis.Remove(predmet);
            }

            context.Studentis.Remove(student_delete);
            context.SaveChanges();
            listView1.FocusedItem.Remove();
        }

        private void detailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Studenti student = (Studenti)listView1.FocusedItem.Tag;
            studentForm studentFrm = new studentForm(student, true);
            studentFrm.ShowDialog(this);
        }

        private void subjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PredmetiForm predmetiForm = new PredmetiForm();
            predmetiForm.Show(this);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Studenti student = (Studenti)listView1.FocusedItem.Tag;
            studentForm studentFrm = new studentForm(student, false);
            studentFrm.ShowDialog(this);

            if (studentFrm.DialogResult == DialogResult.OK)
            {
                student.Ime = studentFrm.getNameInput();
                student.Prezime = studentFrm.getLastNameInput();

                var context = new Entities();
                var studentInChange = context.Studentis.First(s => s.Id == student.Id);
                studentInChange.Ime = student.Ime;
                studentInChange.Prezime = student.Prezime;
                context.SaveChanges();

                listView1.FocusedItem.SubItems.Clear();
                listView1.FocusedItem.SubItems.Add(student.Ime);
                listView1.FocusedItem.SubItems.Add(student.Prezime);
                listView1.FocusedItem.Tag = student;
            }

        }
    }
}
